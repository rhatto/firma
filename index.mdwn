[[!meta title="Firma: encrypted mailing list manager"]]

* Project page: [https://firma.sarava.org](https://firma.sarava.org)
* Issue tracker: [https://firma.sarava.org/trac](https://firma.sarava.org/trac)
* [Changelog](CHANGELOG)
* [License](COPYING): GNU GPL v2
* [Guidelines](GUIDELINES)
* Similar project: [schleuder](http://schleuder2.nadir.org)
* Contact: `firma (@) firma.sarava.org`
* Public key: `gpg --keyserver keys.indymedia.org --recv-keys 3DF104314023E18358EFDD270D51B4E79E693CF7`

**NOTE:** Firma development is currently on hold and **we are looking for a maintainer** :)

Index
-----

[[!toc levels=4]]


Getting the code
----------------

    git clone git://git.sarava.org/firma.git

About
-----

Firma is based on the [gpgmailalias.pl perl script](http://www.rediris.es/app/pgplist/index.en.html),
but completely rewritten in bash.

Firma works as a command line and MTA pipe alias tool that receives an email
message in its input, grab the encrypted/signed message, re-encrypt and send it
to each subscriber.

In the server you just need the script, a keyring with the list keypair and two
config files. When mail arrives, it is redirected by the MTA to the script and
it process the message if its properly encrypted and signed.  No temporary
files are written during a message processing and no default message archive,
so no messages -- encrypted or not -- rest on your system in all steps of the
process.

Architecture
------------

When using a encrypted mailing list software, one must choose between keeping
the private key in the server or send it to each of the subscribers. We'll not
consider the case where every subscribers encrypt the message to all recipients
cause this has none automation in the process we are looking for.

For the first there are some options:

- [Schleuder](http://schleuder2.nadir.org/)
- [GPG Mailman](http://medien.informatik.uni-ulm.de/~stefan/gpg-mailman.xhtml)
- [Crypt-ML - gpg-ezmlm](http://www.synacklabs.net/projects/crypt-ml/)
- [Secure Email List Services (SELS)](http://sels.ncsa.illinois.edu/)

For the second option there is the
[NAH6 Mailman patch](http://mail.python.org/pipermail/mailman-coders/2003-June/000506.html).

During the initiral Firma releases, we have chosen to use just the first
option.  In the future the code should contain support for an one-keypair list,
but this is not the main behavior we want in an encrypted mailing list.  This
is a question of centralized versus decentralized vulnerability.

An one-keypair list is more or less just like a mail alias: someone
send an encrypted email to the list address and the manager just forwards
that encrypted email to the lists subscribers, optionally removing some
headers and performing some security auditing. Every user has the list
private key, so if someone lost it to the world then one must regenerate
a keypair and send again to every subscriber.

In the case of a keypair stored in the server, where subscribers has just
the list pubkey, the list admin just need to remove the correspondent pubkey
from the list's keyring with in case some user has its keypair compromised.

These two approaches has a similar external hole of some private key turned
public. Designing Firma, we decided for the centralized model, for three main
reasons:

1. A server can be safer than any user's ordinary computer.

2. Automation: when some user is removed from the list, we just remove their
   key from the list keyring; in the decentralized approach, the list admin needs
   to regenerate a new list keypair, otherwise the unsubscribed user can still
   decrypt list messages if he can sniff the mail server traffic.

3. Use a public-key and all info stored onto it as the subscriber info.

Why Bash
--------

You may ask why we choose bash. Its really strange a crypto project using
shell scripting language. But bash has many advantages:

- Bash is found in almost all unix-like systems

- Small dependencies: firma needs just tools like sed, awk, grep, cut and
  gpg itself. Look at the file [[GUIDELINES]] to see a complete list of all
  unix commands needed to run firma.

- You can easily put all the tools, scripts and config files in a read-only
  media to protect against cracks such as rootkits.

- Keeping your encrypted list manager out from a huge and sometimes bugged
  mail software prevents insecure use of your mailing list by an excess of
  unwanted functions and routines.

- Firma has a total KISS design, and bash helps to keep it simple.

- Firma adopted the style suggested in the
  [Advanced Bash-Scripting Guide](http://www.tldp.org/LDP/abs/html/scrstyle.html).

Development Guidelines
----------------------

As a security project we made a restricted set of guidelines / design policy,
attempting to get a clean, bug-free and high quality code. We choose a slow
development rather than sit-and-code. These rules are detailed in the file
[[GUIDELINES]].

Setup
-----

Note for Debian users: you'll need the "expect" package to run firma.

Firma installation is quite simple:

### List folder

Create a folder to store lists; by default firma use `/var/lib/firma/lists` but
you can use anything, just edit firma and change `FIRMA_LIST_PATH` variable.

### Script installation

Copy firma script to whatever you like, e.g. `/usr/local/bin` and check that it
has no write permission

### Global config

Create a system-wide config file (default is `/var/lib/firma/firma.conf`) with
the common definitions for all lists. You might just copy the sample
firma.conf.dist and edit according to your needs.

All config variables can be overwritten at each list's own config file;
firma.conf should be chmoded as `600`, chowned `nobody.nobody` or whatever user
your MTA runs. If you run postfix, the user is specified by the `main.cf`
parameter `default_privs`.

For a list of all config parameters, type

       firma --help config

### List creation

Then create your lists with the command

       firma -c your-list

Firma will ask some questions and create a gpg keyring and a config file with
the following variables:

       LIST_ADDRESS= list's email address
       LIST_ADMIN= list's administrators email addresses (space separated)
       LIST_HOMEDIR= list's GnuPG homedir, where the list's keyrings are located
       PASSPHRASE= passphrase for the list's private keyring

A gpg keypair and a config file are automatically generated; the owner of the
config file and keyring should be `nobody.nobody` (or the user your MTA run as)
and its permissions must be `600`.

After that you can add some optional parameters on this list config file:

       SUBJECT_PREFIX= prefix to be included in the subject of list messages

       REMOVE_THESE_HEADERS= headers that should be stripped from list messages
                             (space separated case-insensitive entries)
                             (may include regexps (e.g., X-.*)

       REPLIES_SHOULD_GO_TO_LIST= set to '1' to add a Reply-To header containing the
                                  list address

       SILENTLY_DISCARD_INVALID_MESSAGES= set to '1' to silently discard invalid
                                          messages (message not signed/encrypted,
                                          sender not subscribed to the list, etc.)
                                          instead of sending bounces back to sender

       KEYSERVER= default keyserver to import/export keys
                  (defaults to keyserver.noreply.org)

       REQUIRE_SIGNATURE= whether messages sent to the list should be (1) or don't
                          need to be (0) signed to be processed; defaults to '1';
                          this doesn't affect the way email administration works,
                          when signature is mandatory

For a list of all config parameters, type

       firma --help config

### MTA configuration

Create an alias to the list at your MTA; on sendmail or postfix, add this to
your aliases file:

       your-list: "| /usr/local/bin/firma -p your-list"
       your-list-request: "| /usr/local/bin/firma -e your-list"

and then run the command

       newaliases

alternatively, you can use a virtual mailbox table if you want
to easily host a lot of encrypted mailing lists.

### List administration

Admin tasks are performed through aliases like your-list-request@yourmachine
or via command-line:

       firma -a your-list

Inside this command or encrypted in your mailing list request, use the
following commands:

       sub keyserver|keyfile key-id

         subscribe key-id pubkey from file or keyserver (currently not
         implemented)

       unsub email-address

          unsubscribe all keys with email-address IDs (currently not
          implemented)

       use email-address

          uses the given address for message delivery instead
          of the primary address of a subscribed key

### Subscription management

To subscribe and unsubscribe manually the users and the list admins on, use
a command line like

       gpg --homedir [path-to-your-list-keyring] --import < file

and make sure that after this command the list keyring is owned by `nobody.nobody`
(or the user your MTA run as).

### Testing the installation

Send encrypted AND signed messages to `your-list@yourmachine` and look
what happens :)

Tips
----

- Use an encrypted swap memory
- Use a read-only media to store firma and its needed apps
- Use ramdisk to `FIRMA_LIST_PATH` so all keys and passwords vanishes if the server is shutdown
- Use a big `PASSPHRASE`, 25+ chars with alpha-numeric and special ascii keys

Design and features
-------------------

Firma is simple but its simplicity doesn't reflect in lack of design.

- Uses a gpg keyring to store both the keys and the subscribers options

- Simple command line to avoid admin tasks resting in some `.bash_history`

- Non-pgp blocks in a message are discarded since we don't want to deal with
  unencrypted content

- All unwanted email headers are striped as a privacy measure for who sends
  the message

- Firma doesn't use any disk write when processing a message; no temp files
  that may rest in the system; everything goes in memory (but take care,
  sometimes it will use the swap and then is best to make it encrypted)

- By default it doesn't archive messages in the server

- By default it removes the `Subject` header and put it inside the encrypted
  message, as Subject are outside the PGP/MIME context

- Messages appear to be sent `To: Undisclosed Recipients`

- Administration through email or command-line
