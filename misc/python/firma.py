#!/usr/bin/python
""" Firma - Encrypted Mailing List Manager - Python Version

firma: GnuPG-based encrypted mailing list manager
Feedback: firma@sarava.org

  Firma is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 2 of the License, or (at your option) any later
  version.

  Firma is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
  Place - Suite 330, Boston, MA 02111-1307, USA

Lets use: http://docs.python.org/lib/module-email.html
          http://docs.python.org/lib/module-smtplib.html
          http://py-gnupg.sourceforge.net/

Lets split the work:

 - create list
   - create folder, set permissions
   - create config file
   - create keyring, set permissions again
 - admin list
   - parse config file
   - from command line
     - parse comand line args
     - exec admin tasks
     - display output
   - from email
     - decrypt message
     - parse command line args
     - exec admin tasks
     - record output
     - encrypt and send back output
 - process message
   - parse config file
   - read stdin and store as original message
   - decrypt message
   - if encrypted and signed, encrypt again and send to all subscribers

"""

import GnuPGInterface

# first we'll create just a routine that sets up a new gpg keyring

class NewKey(GnuPGInterface.GnuPG):
	""" Class used to create a new gpg keyring """
	def __init__(self):
		GnuPGInterface.GnuPG.__init__(self)
	        self.options.armor = 0
        	self.options.extra_args.append("--gen-key")
                self.options.meta_interactive = 0

	def __setitem__(self, key, value):
		if key == "homedir" and value:
                        # TODO: check if --homedir already is on self.options.extra_args
			# TODO: check if the folder referenced by "value" exists
			self.options.extra_args.append("--homedir %s" % value)
		# TODO: else raises an exception?

class DelKey(GnuPGInterface.GnuPG):
	""" Class used to delete a key from a keyring """
	def __init__(self):
		GnuPGInterface.GnuPG.__init__(self)

class RevokeKey(GnuPGInterface.GnuPG):
	""" Class used to revoke a key """
	def __init__(self):
		GnuPGInterface.GnuPG.__init__(self)

# lets start the code

passwd = "senha"
newkey = NewKey()
newkey = ["homedir"] = "firma-python/keyring-teste" 

print newkey.options.extra_args
